package com.dev.marioponce.gnbapp.view

import com.dev.marioponce.gnbapp.model.TransactionsViewModel

/**
 * Transactions view interface.
 */
interface TransactionsView {
    /**
     * Draw product selector.
     * @param products products.
     */
    fun drawProductSelector(products: ArrayList<String?>)
    /**
     * Draw product selector.
     * @param transactions transactions to show.
     */
    fun drawTransactions(transactions: TransactionsViewModel)

    /**
     * Use generic error method function.
     */
    fun drawError()
}