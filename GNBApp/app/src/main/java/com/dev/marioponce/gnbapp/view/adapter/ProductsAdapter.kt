package com.dev.marioponce.gnbapp.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.dev.marioponce.gnbapp.R
import kotlinx.android.synthetic.main.product_item.view.*

class ProductsAdapter(
    private val context: Context?,
    private val products: ArrayList<String?>,
    private val listener: ProductsAdapterListener?)
    : RecyclerView.Adapter<ProductsAdapter.ProductViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ProductsAdapter.ProductViewHolder {
        return ProductsAdapter.ProductViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.product_item,
                viewGroup,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return products.size
    }

    override fun onBindViewHolder(holder: ProductsAdapter.ProductViewHolder, position: Int) {
        holder.button.text = products[position]
        holder.button.setOnClickListener { listener?.onProductClicked(product = products[position]!!) }
    }

    class ProductViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val button: Button = view.product_button
    }

    interface ProductsAdapterListener {
        fun onProductClicked(product: String)
    }
}