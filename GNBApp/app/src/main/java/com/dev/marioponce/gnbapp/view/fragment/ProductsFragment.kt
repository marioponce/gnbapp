package com.dev.marioponce.gnbapp.view.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.dev.marioponce.gnbapp.R
import com.dev.marioponce.gnbapp.view.adapter.ProductsAdapter
import kotlinx.android.synthetic.main.fragment_products.*


/**
 * Products fragment.
 */

class ProductsFragment : Fragment() {

    private var products: ArrayList<String?> = ArrayList()
    private var listener: ProductsAdapter.ProductsAdapterListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            products = it.getStringArrayList(ARG_PRODUCTS) as ArrayList<String?>
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_products, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.initAdapter()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ProductsAdapter.ProductsAdapterListener) {
            listener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun initAdapter() {
        val adapter = ProductsAdapter(context, products, listener)
        products_recycler.layoutManager = GridLayoutManager(context, ADAPTER_SPAN_COUNT)
        products_recycler.adapter = adapter
    }

    companion object {

        const val TAG = "ProductsFragment"
        private const val ARG_PRODUCTS = "products"
        private const val ADAPTER_SPAN_COUNT = 2

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param products Product list
         *
         * @return A new instance of fragment BlankFragment.
         */
        @JvmStatic
        fun newInstance(products: ArrayList<String?>) = ProductsFragment().apply {
            arguments = Bundle().apply {
                putStringArrayList(ARG_PRODUCTS, products)
            }
        }
    }
}
