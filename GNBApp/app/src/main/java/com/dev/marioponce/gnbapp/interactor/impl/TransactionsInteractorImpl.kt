package com.dev.marioponce.gnbapp.interactor.impl

import android.content.Context
import com.dev.marioponce.gnbapp.interactor.TransactionsInteractor
import com.dev.marioponce.gnbapp.model.RateModel
import com.dev.marioponce.gnbapp.model.TransactionModel
import com.dev.marioponce.gnbapp.model.TransactionsViewModel
import com.dev.marioponce.gnbapp.repository.TransactionsRepository
import com.dev.marioponce.gnbapp.repository.impl.TransactionsRepositoryImpl
import com.dev.marioponce.gnbapp.utils.GNBConstants

class TransactionsInteractorImpl(context: Context) : TransactionsInteractor {

    private var repository: TransactionsRepository = TransactionsRepositoryImpl(context)

    private lateinit var transactions: List<TransactionModel>
    private lateinit var rates: List<RateModel>

    // INTERACTOR INTERFACE METHODS
    override fun getProducts(
        result: (products: ArrayList<String?>) -> Unit,
        error: () -> Unit
    ) {
        repository.run {
            getTransactions({
                transactions = it
                val products: ArrayList<String?> = ArrayList()
                for (transaction in transactions) {
                    if (!products.contains(transaction.sku)) {
                        products.add(transaction.sku)
                    }
                }
                result(products)
            }, error)
            getRates({
                rates = it
            }, error)
        }
    }

    override fun getTransactions(
        product: String,
        result: (transactions: TransactionsViewModel) -> Unit,
        error: () -> Unit
    ) {
        val transactionsResult: ArrayList<TransactionModel> = ArrayList()
        var totalAmount = 0.0
        for (transaction in transactions) {
            if (transaction.sku == product) {
                when {
                    transaction.currency.equals(GNBConstants.EUR) -> transactionsResult.add(transaction)
                    getDirectRate(transaction.currency, GNBConstants.EUR) != null -> transactionsResult.add(
                        convert(transaction, getDirectRate(transaction.currency, GNBConstants.EUR), GNBConstants.EUR))
                    else -> {
                        transactionsResult.add(getAlternativeConvertToEUR(transaction))
                    }
                }
                totalAmount += transaction.amount!!.toDouble()
            }
        }
        val transactionsViewModel = TransactionsViewModel(totalAmount, product, transactionsResult)
        result(transactionsViewModel)
    }

    /**
     * This method find an alternative conversion to EUR iterating himself between the currencies till it get the way to
     * convert it. First search for all the rates to EUR and try to convert the value to the "From" value of this conversion.
     * If it doesn't have that conversion, it will start to iterate using all the rates from our start currency that we have.
     * @param transaction Transaction model
     * @return Transaction model to EUR
     */
    private fun getAlternativeConvertToEUR(transaction: TransactionModel): TransactionModel {
        val ratesToEUR = rates.filter { it.to == GNBConstants.EUR }
        var convertedTransaction: TransactionModel

        // Check if we have the rate value from our currency to the "From" value of the EUR conversion.
        for (rate in ratesToEUR) {
            if (getDirectRate(transaction.currency, rate.from) != null) {
                convertedTransaction = convert(transaction, getDirectRate(transaction.currency, rate.from), rate.from)
                return convert(convertedTransaction, getDirectRate(convertedTransaction.currency, GNBConstants.EUR),
                    GNBConstants.EUR)
            }
        }

        val ratesFromOurCurrency = rates.filter { it.from == transaction.currency }

        for (rate in ratesFromOurCurrency) {
            convertedTransaction = convert(transaction, getDirectRate(transaction.currency, rate.to), rate.to)
            return getAlternativeConvertToEUR(convertedTransaction)
        }

        return transaction
    }

    /**
     * Find the rate value on our list using the params. If the rate can't be found it return null.
     * @param from From currency
     * @param to To currency
     * @return Rate or null if that rate can't be found
     */
    private fun getDirectRate(from: String?, to: String?): String? {
        val rate: RateModel? = rates.find { it.from == from && it.to == to }
        return rate?.rate
    }

    /**
     * Return a transaction model with the new amount and currency values.
     * @param transaction transaction model
     * @param rate rate value
     * @param currency currency value
     * @return transaction model
     */
    private fun convert(transaction: TransactionModel, rate: String?, currency: String?): TransactionModel {
        transaction.currency = currency
        transaction.amount = parseAmount(transaction.amount, rate)
        return transaction
    }

    /**
     * Convert an amount value into another based on a rate.
     * @param amount amount value
     * @param rate rate value
     * @return parse amount with rate received as a param
     */
    private fun parseAmount(amount: String?, rate: String?): String? {
        return (amount!!.toDouble() * rate!!.toDouble()).toString()
    }
}