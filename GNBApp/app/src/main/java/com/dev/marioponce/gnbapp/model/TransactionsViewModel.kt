package com.dev.marioponce.gnbapp.model

import android.os.Parcel
import android.os.Parcelable

class TransactionsViewModel(
    val totalAmount: Double,
    val product: String?,
    val transactions: ArrayList<TransactionModel>): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readDouble(),
        parcel.readString(),
        arrayListOf<TransactionModel>().apply {
            parcel.readList(this, TransactionModel::class.java.classLoader)
        }
    )
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(totalAmount)
        parcel.writeString(product)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TransactionsViewModel> {
        override fun createFromParcel(parcel: Parcel): TransactionsViewModel {
            return TransactionsViewModel(parcel)
        }

        override fun newArray(size: Int): Array<TransactionsViewModel?> {
            return arrayOfNulls(size)
        }
    }
}