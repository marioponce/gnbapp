package com.dev.marioponce.gnbapp.utils

object GNBConstants {
    const val GNB_URL_TRANSACTIONS = "http://quiet-stone-2094.herokuapp.com/transactions.json"
    const val GNB_URL_RATES = "http://quiet-stone-2094.herokuapp.com/rates.json"
    const val RESULT_OK = "REQUEST OK: "
    const val RESULT_KO = "REQUEST KO: "
    const val EUR = "EUR"
}