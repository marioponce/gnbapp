package com.dev.marioponce.gnbapp.repository.impl

import android.content.Context
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.dev.marioponce.gnbapp.model.RateModel
import com.dev.marioponce.gnbapp.model.TransactionModel
import com.dev.marioponce.gnbapp.repository.TransactionsRepository
import com.dev.marioponce.gnbapp.utils.GNBConstants
import com.google.gson.Gson

class TransactionsRepositoryImpl(private val context: Context) : TransactionsRepository {

    private val TAG = "TransactionsRepository"

    // REPOSITORY INTERFACE METHODS
    override fun getTransactions(
        result: (products: List<TransactionModel>) -> Unit,
        error: () -> Unit
    ) {
        val queue = Volley.newRequestQueue(context)
        val stringRequest = StringRequest(
            Request.Method.GET, GNBConstants.GNB_URL_TRANSACTIONS,
            Response.Listener<String> { response ->
                Log.v(TAG, GNBConstants.RESULT_OK + response.toString())
                val transactionResult: List<TransactionModel> = Gson().fromJson(
                    response.toString(),
                    Array<TransactionModel>::class.java
                ).toList()
                result(transactionResult)
            },
            Response.ErrorListener {
                Log.v(TAG, GNBConstants.RESULT_KO + it.message)
                it.message?.let {error()}
            })
        queue.add(stringRequest)
    }

    override fun getRates(
        result: (products: List<RateModel>) -> Unit,
        error: () -> Unit
    ) {
        val queue = Volley.newRequestQueue(context)
        val stringRequest = StringRequest(
            Request.Method.GET, GNBConstants.GNB_URL_RATES,
            Response.Listener<String> { response ->
                Log.v(TAG, GNBConstants.RESULT_OK + response.toString())
                val rateResult: List<RateModel> = Gson().fromJson(
                    response.toString(),
                    Array<RateModel>::class.java
                ).toList()
                result(rateResult)
            },
            Response.ErrorListener {
                Log.v(TAG, GNBConstants.RESULT_KO + it.message)
                it.message?.let { error() }
            })
        queue.add(stringRequest)
    }
}