package com.dev.marioponce.gnbapp.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.dev.marioponce.gnbapp.R
import com.dev.marioponce.gnbapp.model.TransactionModel
import com.dev.marioponce.gnbapp.utils.AmountFormatterUtils
import kotlinx.android.synthetic.main.transaction_item.view.*

class TransactionsAdapter(
    private val context: Context?,
    private val transactions: ArrayList<TransactionModel>)
    : RecyclerView.Adapter<TransactionsAdapter.TransactionViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): TransactionsAdapter.TransactionViewHolder {
        return TransactionsAdapter.TransactionViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.transaction_item,
                viewGroup,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return transactions.size
    }

    override fun onBindViewHolder(holder: TransactionsAdapter.TransactionViewHolder, position: Int) {
        holder.amount.text = AmountFormatterUtils.formatAmount(transactions[position].amount!!.toDouble())
        holder.currency.text = context?.getString(R.string.currency_eur)
    }

    class TransactionViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val amount: TextView = view.text_amount_value
        val currency: TextView = view.text_currency
    }
}