package com.dev.marioponce.gnbapp.model

data class RateModel(
    val from: String,
    val to: String,
    val rate: String
)