package com.dev.marioponce.gnbapp.utils

import java.text.DecimalFormat

class AmountFormatterUtils {
    companion object {
        fun formatAmount(value: Double): String {
            val df = DecimalFormat("#.## ")
            df.minimumFractionDigits = 2
            return df.format(value)
        }
    }
}