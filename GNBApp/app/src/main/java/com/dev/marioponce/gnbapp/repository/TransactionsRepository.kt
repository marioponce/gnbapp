package com.dev.marioponce.gnbapp.repository

import com.dev.marioponce.gnbapp.model.RateModel
import com.dev.marioponce.gnbapp.model.TransactionModel

interface TransactionsRepository {
    fun getTransactions(result: (products: List<TransactionModel>) -> Unit,
                        error: () -> Unit)
    fun getRates(result: (products: List<RateModel>) -> Unit,
                 error: () -> Unit)
}