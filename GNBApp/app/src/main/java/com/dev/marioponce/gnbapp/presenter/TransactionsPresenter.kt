package com.dev.marioponce.gnbapp.presenter

import com.dev.marioponce.gnbapp.view.TransactionsView

interface TransactionsPresenter {
    fun setView(view: TransactionsView)
    fun productSelector()
    fun transactions(product: String)
}