package com.dev.marioponce.gnbapp.view.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.dev.marioponce.gnbapp.R
import com.dev.marioponce.gnbapp.model.TransactionsViewModel
import com.dev.marioponce.gnbapp.presenter.TransactionsPresenter
import com.dev.marioponce.gnbapp.presenter.impl.TransactionsPresenterImpl
import com.dev.marioponce.gnbapp.view.TransactionsView
import com.dev.marioponce.gnbapp.view.adapter.ProductsAdapter
import com.dev.marioponce.gnbapp.view.fragment.ProductsFragment
import com.dev.marioponce.gnbapp.view.fragment.TransactionsFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), TransactionsView, ProductsAdapter.ProductsAdapterListener {

    private var presenter: TransactionsPresenter = TransactionsPresenterImpl(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.setView(this)
        productSelector()
    }

    override fun drawProductSelector(products: ArrayList<String?>) {
        hideLoading()
        supportFragmentManager
            .beginTransaction()
            .add(R.id.fragment, ProductsFragment.newInstance(products), ProductsFragment.TAG)
            .commitAllowingStateLoss()
    }

    // VIEW INTERFACE METHODS
    override fun drawTransactions(transactions: TransactionsViewModel) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment, TransactionsFragment.newInstance(transactions), TransactionsFragment.TAG)
            .addToBackStack(TransactionsFragment.TAG)
            .commitAllowingStateLoss()
    }

    override fun drawError() {
        hideLoading()
        buildAlertError()
    }

    // FRAGMENT LISTENER
    override fun onProductClicked(product: String) {
        presenter.transactions(product)
    }

    private fun productSelector() {
        showLoading()
        presenter.productSelector()
    }

    private fun buildAlertError() {
        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setTitle(getString(R.string.alert_dialog_error_title))
        builder.setMessage(getString(R.string.alert_dialog_error_text))
        builder.setPositiveButton(getString(R.string.alert_dialog_error_yes)){ _, _ ->
            productSelector()
        }
        builder.setNegativeButton(getString(R.string.alert_dialog_error_no)){ _, _ ->

        }
        builder.setNeutralButton(getString(R.string.alert_dialog_error_leave)){_,_ ->
            finish()
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun showLoading() {
        loading.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        loading.visibility = View.GONE
    }
}
