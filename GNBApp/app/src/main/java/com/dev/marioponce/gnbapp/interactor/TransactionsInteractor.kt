package com.dev.marioponce.gnbapp.interactor

import com.dev.marioponce.gnbapp.model.TransactionsViewModel

interface TransactionsInteractor {
    fun getProducts(result: (products: ArrayList<String?>) -> Unit, error: () -> Unit)
    fun getTransactions(product: String, result: (transactions: TransactionsViewModel) -> Unit,
                        error: () -> Unit)
}