package com.dev.marioponce.gnbapp.presenter.impl

import android.content.Context
import com.dev.marioponce.gnbapp.interactor.TransactionsInteractor
import com.dev.marioponce.gnbapp.interactor.impl.TransactionsInteractorImpl
import com.dev.marioponce.gnbapp.presenter.TransactionsPresenter
import com.dev.marioponce.gnbapp.view.TransactionsView

/**
 * Transactions presenter implementation.
 */
class TransactionsPresenterImpl(context: Context) : TransactionsPresenter {

    private lateinit var view: TransactionsView
    private var interactor: TransactionsInteractor = TransactionsInteractorImpl(context)

    override fun setView(view: TransactionsView) {
        this.view = view
    }

    override fun productSelector() {
        interactor.getProducts(view::drawProductSelector, view::drawError)
    }

    override fun transactions(product: String) {
        interactor.getTransactions(product ,view::drawTransactions, view::drawError)
    }

}