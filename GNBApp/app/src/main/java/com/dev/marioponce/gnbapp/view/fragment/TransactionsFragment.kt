package com.dev.marioponce.gnbapp.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.dev.marioponce.gnbapp.R
import com.dev.marioponce.gnbapp.model.TransactionsViewModel
import com.dev.marioponce.gnbapp.utils.AmountFormatterUtils
import com.dev.marioponce.gnbapp.view.adapter.TransactionsAdapter
import kotlinx.android.synthetic.main.fragment_transactions.*

/**
 * Transactions fragment
 */
class TransactionsFragment : Fragment() {

    private lateinit var transactions: TransactionsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            transactions = it.getParcelable(ARG_TRANSACTIONS) as TransactionsViewModel
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transactions, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        setData()
    }

    private fun setData() {
        transactions_product.text = transactions.product
        transactions_total_amount_value.text = AmountFormatterUtils.formatAmount(transactions.totalAmount)
    }

    private fun initAdapter() {
        val adapter = TransactionsAdapter(context, transactions.transactions)
        val layoutManager = LinearLayoutManager(context)
        val dividerItemDecoration = DividerItemDecoration(
            transactions_recycler.context,
            layoutManager.orientation
        )
        transactions_recycler.addItemDecoration(dividerItemDecoration)
        transactions_recycler.layoutManager = layoutManager
        transactions_recycler.adapter = adapter
    }

    companion object {

        const val TAG = "TransactionsFragment"
        private const val ARG_TRANSACTIONS = "products"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param transactions transactions.
         * @return A new instance of fragment TransactionsFragment.
         */
        @JvmStatic
        fun newInstance(transactions: TransactionsViewModel) = TransactionsFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_TRANSACTIONS, transactions)
                }
            }
    }
}
